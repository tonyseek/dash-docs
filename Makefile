help:
	@echo "You need to set up the correct FOO_SOURCE variable first."
	@echo
	@echo "- MARSHMALLOW_SOURCE=/path/to/src make Marshmallow.docset"
.PHONY: help

Marshmallow.docset: $(MARSHMALLOW_SOURCE)
	cd $(MARSHMALLOW_SOURCE)/docs && pip install -r requirements.txt
	cd $(MARSHMALLOW_SOURCE)/docs && $(MAKE) html
	doc2dash --name=Marshmallow \
			 --index-page='index.html' --force \
			 --icon='$(MARSHMALLOW_SOURCE)/docs/_static/marshmallow-logo.png' \
			 $(MARSHMALLOW_SOURCE)/docs/_build/html
	rm -rf Marshmallow.docset/Contents/Resources/Documents/.venv
	rm -rf Marshmallow.docset/Contents/Resources/Documents/_sources
.PHONY: Marshmallow.docset


Click.docset: $(CLICK_SOURCE)
	cd $(CLICK_SOURCE) && pip install .
	cd $(CLICK_SOURCE)/docs && $(MAKE) html
	doc2dash --name=Click \
			 --index-page='index.html' --force \
			 --icon='$(CLICK_SOURCE)/docs/_static/click.png' \
			 --online-redirect-url='http://click.pocoo.org/6/' \
			 $(CLICK_SOURCE)/docs/_build/html
	rm -rf Click.docset/Contents/Resources/Documents/.venv
	rm -rf Click.docset/Contents/Resources/Documents/_sources
.PHONY: Click.docset

Gevent.docset: $(GEVENT_SOURCE)
	cd $(GEVENT_SOURCE) && pip install sphinx && pip install .
	cd $(GEVENT_SOURCE)/doc && $(MAKE) html
	doc2dash --name=Gevent \
			 --index-page='contents.html' --force \
			 --online-redirect-url='http://www.gevent.org' \
			 $(GEVENT_SOURCE)/doc/_build/html
	rm -rf Gevent.docset/Contents/Resources/Documents/.venv
	rm -rf Gevent.docset/Contents/Resources/Documents/_sources
.PHONY: Gevent.docset

Kazoo.docset: $(KAZOO_SOURCE)
	rm -rf $(KAZOO_SOURCE)/docs/_build/html
	cd $(KAZOO_SOURCE) \
		&& $(KAZOO_SOURCE)/bin/pip install . \
		&& $(KAZOO_SOURCE)/bin/pip install -r requirements.txt \
		&& $(KAZOO_SOURCE)/bin/pip install -r requirements_eventlet.txt \
		&& $(KAZOO_SOURCE)/bin/pip install -r requirements_gevent.txt \
		&& $(KAZOO_SOURCE)/bin/pip install -r requirements_sphinx.txt \
		&& make html
	doc2dash --name=Kazoo \
			 --index-page='index.html' --force \
			 --online-redirect-url='https://kazoo.readthedocs.io' \
			 $(KAZOO_SOURCE)/docs/_build/html
.PHONY: Kazoo.docset

Tox.docset: $(TOX_SOURCE)
	rm -rf $(TOX_SOURCE)/doc/_build/html
	cd $(TOX_SOURCE) \
		&& $(TOX_SOURCE)/venv/bin/pip install . \
		&& $(TOX_SOURCE)/venv/bin/pip install -U sphinx \
		&& . $(TOX_SOURCE)/venv/bin/activate \
		&& make -C doc html
	doc2dash --name=Tox \
			 --index-page='index.html' --force \
			 --online-redirect-url='https://testrun.org/tox/' \
			 $(TOX_SOURCE)/doc/_build/html
.PHONY: Tox.docset

WTForms.docset: $(WTFORMS_SOURCE)
	rm -rf $(WTFORMS_SOURCE)/docs/_build/html
	cd $(WTFORMS_SOURCE) \
		&& $(WTFORMS_SOURCE)/env/bin/pip install '.[Locale]' \
		&& $(WTFORMS_SOURCE)/env/bin/pip install python-dateutil \
		&& $(WTFORMS_SOURCE)/env/bin/pip install -U sphinx \
		&& . $(WTFORMS_SOURCE)/env/bin/activate \
		&& make -C docs html
	doc2dash --name=WTForms \
			 --index-page='index.html' --force \
			 --online-redirect-url='http://wtforms.readthedocs.io/en/2.1/' \
			 $(WTFORMS_SOURCE)/docs/_build/html
.PHONY: WTForms.docset

urllib3.docset: $(URLLIB3_SOURCE)
	cd $(URLLIB3_SOURCE) && tox -e docs
	doc2dash --name=urllib3 \
			 --index-page='index.html' --force \
			 --online-redirect-url='https://urllib3.readthedocs.io/en/stable/' \
			 $(URLLIB3_SOURCE)/docs/_build/html
.PHONY: urllib3.docset

pytest.docset: $(PYTEST_SOURCE)
	cd $(PYTEST_SOURCE) \
		&& tox -e doc
	doc2dash --name=pytest \
			 --index-page='index.html' --force \
			 --online-redirect-url='http://pytest.org/latest/' \
			 $(PYTEST_SOURCE)/doc/en/_build/html
.PHONY: pytest.docset

iproute2.docset:
	$(eval BUILD_DIR := $(shell mktemp -d))
	cd $(BUILD_DIR) \
		&& wget http://ftp.cn.debian.org/debian/pool/main/i/iproute2/iproute2-doc_3.16.0-2_all.deb \
		&& wget https://kapeli.com/resources/Info.plist \
		&& ar -x iproute2-doc_3.16.0-2_all.deb \
		&& tar -zxf data.tar.xz \
		&& mkdir -p iproute2.docset/Contents/Resources/Documents/ \
		&& find usr/share/doc/iproute2-doc -type f -name '*.html' -exec sh -c \
				'mkdir -p "iproute2.docset/Contents/Resources/Documents/$$(basename "{}" ".html")" \
				&& cp "{}" "iproute2.docset/Contents/Resources/Documents/$$(basename "{}" ".html")"/index.html ' \
				\; \
		&& sed 's/nginx/iproute2/g; s/Nginx/iproute2/g' Info.plist | \
				awk '/<\/dict>/ {print "<key>DashDocSetFallbackURL</key>\n<string>http://linux-ip.net/gl/</string>"}1' > \
				iproute2.docset/Contents/Info.plist \
		&& sqlite3 docSet.dsidx \
			"CREATE TABLE searchIndex(id INTEGER PRIMARY KEY, name TEXT, type TEXT, path TEXT);" \
			"CREATE UNIQUE INDEX anchor ON searchIndex (name, type, path);" \
			"INSERT INTO searchIndex(name, type, path) VALUES ('ARPD Daemon', 'Guide', 'arpd/index.html');" \
			"INSERT INTO searchIndex(name, type, path) VALUES ('STAT, IFSTAT and RTACCT Utilities', 'Guide', 'nstat/index.html');" \
			"INSERT INTO searchIndex(name, type, path) VALUES ('RTACCT Utility', 'Guide', 'rtstat/index.html');" \
			"INSERT INTO searchIndex(name, type, path) VALUES ('SS Utility: Quick Intro', 'Guide', 'ss/index.html');" \
		&& mv docSet.dsidx iproute2.docset/Contents/Resources/docSet.dsidx
	rm -rf "iproute2.docset"
	mv "$(BUILD_DIR)/iproute2.docset" "iproute2.docset"
	rm -rf "$(BUILD_DIR)"
.PHONY: iproute2.docset
