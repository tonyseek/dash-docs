<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <title>Why marshmallow? &mdash; marshmallow 2.2.0dev documentation</title>
    
    <link rel="stylesheet" href="_static/alabaster.css" type="text/css" />
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '2.2.0dev',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <link rel="top" title="marshmallow 2.2.0dev documentation" href="index.html" />
    <link rel="next" title="Changelog" href="changelog.html" />
    <link rel="prev" title="API Reference" href="api_reference.html" />
   
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9">

  </head>
  <body role="document">  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="why-marshmallow">
<span id="why"></span><h1>Why marshmallow?<a class="headerlink" href="#why-marshmallow" title="Permalink to this headline">¶</a></h1>
<p>The Python ecosystem has many great libraries for data formatting and schema validation.</p>
<p>In fact, marshmallow was influenced by a number of these libraries. Marshmallow is inspired by <a class="reference external" href="http://www.django-rest-framework.org/">Django REST Framework</a>, <a class="reference external" href="http://flask-restful.readthedocs.org/">Flask-RESTful</a>, and <a class="reference external" href="http://docs.pylonsproject.org/projects/colander/en/latest/">colander</a>. It borrows a number of implementation and design ideas from these libraries to create a flexible and productive solution for marshalling, unmarshalling, and validating data.</p>
<p>Here are just a few reasons why you might use marshmallow.</p>
<div class="section" id="agnostic">
<h2>Agnostic.<a class="headerlink" href="#agnostic" title="Permalink to this headline">¶</a></h2>
<p>Marshmallow makes no assumption about web frameworks or database layers. It will work with just about any ORM, ODM, or no ORM at all. This gives you the freedom to choose the components that fit your application&#8217;s needs without having to change your data formatting code. If you wish, you can build integration layers to make marshmallow work more closely with your frameworks and libraries of choice (for examples, see <a class="reference external" href="https://github.com/marshmallow-code/flask-marshmallow">Flask-Marshmallow</a>, and <a class="reference external" href="http://tomchristie.github.io/django-rest-marshmallow/">Django REST Marshmallow</a>).</p>
</div>
<div class="section" id="concise-familiar-syntax">
<h2>Concise, familiar syntax.<a class="headerlink" href="#concise-familiar-syntax" title="Permalink to this headline">¶</a></h2>
<p>If you have used <a class="reference external" href="http://www.django-rest-framework.org/">Django REST Framework</a> or  <a class="reference external" href="http://wtforms.simplecodes.com/docs/1.0.3/">WTForms</a>, marshmallow&#8217;s <code class="xref py py-class docutils literal"><span class="pre">Schema</span></code> syntax will feel familiar to you. Class-level field attributes define the schema for formatting your data. Configuration is added using the <a class="reference internal" href="quickstart.html#meta-options"><span>class Meta</span></a> paradigm. Configuration options can be overriden at application runtime by passing arguments to the <a class="reference internal" href="api_reference.html#marshmallow.Schema" title="marshmallow.Schema"><code class="xref py py-obj docutils literal"><span class="pre">Schema</span></code></a> constructor. The <a class="reference internal" href="api_reference.html#marshmallow.Schema.dump" title="marshmallow.Schema.dump"><code class="xref py py-meth docutils literal"><span class="pre">dump</span></code></a> and <a class="reference internal" href="api_reference.html#marshmallow.Schema.load" title="marshmallow.Schema.load"><code class="xref py py-meth docutils literal"><span class="pre">load</span></code></a> methods are used for serialization and deserialization (of course!).</p>
</div>
<div class="section" id="class-based-schemas-allow-for-code-reuse-and-configuration">
<h2>Class-based schemas allow for code reuse and configuration.<a class="headerlink" href="#class-based-schemas-allow-for-code-reuse-and-configuration" title="Permalink to this headline">¶</a></h2>
<p>Unlike <a class="reference external" href="http://flask-restful.readthedocs.org/">Flask-RESTful</a>, which uses dictionaries to define output schemas, marshmallow uses classes. This allows for easy code reuse and configuration. It also allows for powerful means for configuring and extending schemas, such as adding <a class="reference internal" href="extending.html#extending"><span>post-processing and error handling behavior</span></a>.</p>
</div>
<div class="section" id="consistency-meets-flexibility">
<h2>Consistency meets flexibility.<a class="headerlink" href="#consistency-meets-flexibility" title="Permalink to this headline">¶</a></h2>
<p>Marshmallow makes it easy to modify a schema&#8217;s output at application runtime. A single <code class="xref py py-class docutils literal"><span class="pre">Schema</span></code> can produce multiple outputs formats while keeping the individual field outputs consistent.</p>
<p>As an example, you might have a JSON endpoint for retrieving all information about a video game&#8217;s state. You then add a low-latency endpoint that only returns a minimal subset of information about game state. Both endpoints can be handled by the same <a class="reference internal" href="api_reference.html#marshmallow.Schema" title="marshmallow.Schema"><code class="xref py py-obj docutils literal"><span class="pre">Schema</span></code></a>.</p>
<div class="highlight-python"><div class="highlight"><pre><span class="k">class</span> <span class="nc">GameStateSchema</span><span class="p">(</span><span class="n">Schema</span><span class="p">):</span>
    <span class="n">_id</span> <span class="o">=</span> <span class="n">fields</span><span class="o">.</span><span class="n">UUID</span><span class="p">(</span><span class="n">required</span><span class="o">=</span><span class="bp">True</span><span class="p">)</span>
    <span class="n">players</span> <span class="o">=</span> <span class="n">fields</span><span class="o">.</span><span class="n">Nested</span><span class="p">(</span><span class="n">PlayerSchema</span><span class="p">,</span> <span class="n">many</span><span class="o">=</span><span class="bp">True</span><span class="p">)</span>
    <span class="n">score</span> <span class="o">=</span> <span class="n">fields</span><span class="o">.</span><span class="n">Nested</span><span class="p">(</span><span class="n">ScoreSchema</span><span class="p">)</span>
    <span class="n">last_changed</span> <span class="o">=</span> <span class="n">fields</span><span class="o">.</span><span class="n">DateTime</span><span class="p">(</span><span class="n">format</span><span class="o">=</span><span class="s">&#39;rfc&#39;</span><span class="p">)</span>

    <span class="k">class</span> <span class="nc">Meta</span><span class="p">:</span>
        <span class="n">additional</span> <span class="o">=</span> <span class="p">(</span><span class="s">&#39;title&#39;</span><span class="p">,</span> <span class="s">&#39;date_created&#39;</span><span class="p">,</span> <span class="s">&#39;type&#39;</span><span class="p">,</span> <span class="s">&#39;is_active&#39;</span><span class="p">)</span>

<span class="c"># Serializes full game state</span>
<span class="n">full_serializer</span> <span class="o">=</span> <span class="n">GameStateSchema</span><span class="p">()</span>
<span class="c"># Serializes a subset of information, for a low-latency endpoint</span>
<span class="n">summary_serializer</span> <span class="o">=</span> <span class="n">GameStateSchema</span><span class="p">(</span><span class="n">only</span><span class="o">=</span><span class="p">(</span><span class="s">&#39;_id&#39;</span><span class="p">,</span> <span class="s">&#39;last_changed&#39;</span><span class="p">))</span>
<span class="c"># Also filter the fields when serializing multiple games</span>
<span class="n">gamelist_serializer</span> <span class="o">=</span> <span class="n">GameStateSchema</span><span class="p">(</span><span class="n">many</span><span class="o">=</span><span class="bp">True</span><span class="p">,</span>
                                      <span class="n">only</span><span class="o">=</span><span class="p">(</span><span class="s">&#39;_id&#39;</span><span class="p">,</span> <span class="s">&#39;players&#39;</span><span class="p">,</span> <span class="s">&#39;last_changed&#39;</span><span class="p">))</span>
</pre></div>
</div>
<p>In this example, a single schema produced three different outputs! The dynamic nature of a <code class="xref py py-class docutils literal"><span class="pre">Schema</span></code> leads to <strong>less code</strong> and <strong>more consistent formatting</strong>.</p>
</div>
<div class="section" id="context-aware-serialization">
<h2>Context-aware serialization.<a class="headerlink" href="#context-aware-serialization" title="Permalink to this headline">¶</a></h2>
<p>Marshmallow schemas can modify their output based on the context in which they are used. Field objects have access to a <code class="docutils literal"><span class="pre">context</span></code> dictionary that can be changed at runtime.</p>
<p>Here&#8217;s a simple example that shows how a <a class="reference internal" href="api_reference.html#marshmallow.Schema" title="marshmallow.Schema"><code class="xref py py-obj docutils literal"><span class="pre">Schema</span></code></a> can anonymize a person&#8217;s name when a boolean is set on the context.</p>
<div class="highlight-python"><div class="highlight"><pre><span class="k">class</span> <span class="nc">PersonSchema</span><span class="p">(</span><span class="n">Schema</span><span class="p">):</span>
    <span class="nb">id</span> <span class="o">=</span> <span class="n">fields</span><span class="o">.</span><span class="n">Integer</span><span class="p">()</span>
    <span class="n">name</span> <span class="o">=</span> <span class="n">fields</span><span class="o">.</span><span class="n">Method</span><span class="p">(</span><span class="s">&#39;get_name&#39;</span><span class="p">)</span>

    <span class="k">def</span> <span class="nf">get_name</span><span class="p">(</span><span class="bp">self</span><span class="p">,</span> <span class="n">person</span><span class="p">,</span> <span class="n">context</span><span class="p">):</span>
        <span class="k">if</span> <span class="n">context</span><span class="o">.</span><span class="n">get</span><span class="p">(</span><span class="s">&#39;anonymize&#39;</span><span class="p">):</span>
            <span class="k">return</span> <span class="s">&#39;&lt;anonymized&gt;&#39;</span>
        <span class="k">return</span> <span class="n">person</span><span class="o">.</span><span class="n">name</span>

<span class="n">person</span> <span class="o">=</span> <span class="n">Person</span><span class="p">(</span><span class="n">name</span><span class="o">=</span><span class="s">&#39;Monty&#39;</span><span class="p">)</span>
<span class="n">schema</span> <span class="o">=</span> <span class="n">PersonSchema</span><span class="p">()</span>
<span class="n">schema</span><span class="o">.</span><span class="n">dump</span><span class="p">(</span><span class="n">person</span><span class="p">)</span>  <span class="c"># {&#39;id&#39;: 143, &#39;name&#39;: &#39;Monty&#39;}</span>

<span class="c"># In a different context, anonymize the name</span>
<span class="n">schema</span><span class="o">.</span><span class="n">context</span><span class="p">[</span><span class="s">&#39;anonymize&#39;</span><span class="p">]</span> <span class="o">=</span> <span class="bp">True</span>
<span class="n">schema</span><span class="o">.</span><span class="n">dump</span><span class="p">(</span><span class="n">person</span><span class="p">)</span>  <span class="c"># {&#39;id&#39;: 143, &#39;name&#39;: &#39;&lt;anonymized&gt;&#39;}</span>
</pre></div>
</div>
<div class="admonition seealso">
<p class="first admonition-title">See also</p>
<p class="last">See the relevant section of the <a class="reference internal" href="custom_fields.html#adding-context"><span>usage guide</span></a> to learn more about context-aware serialization.</p>
</div>
</div>
<div class="section" id="advanced-schema-nesting">
<h2>Advanced schema nesting.<a class="headerlink" href="#advanced-schema-nesting" title="Permalink to this headline">¶</a></h2>
<p>Most serialization libraries provide some means for nesting schemas within each other, but they often fail to meet common use cases in clean way. Marshmallow aims to fill these gaps by adding a few nice features for <a class="reference internal" href="nesting.html#nesting"><span>nesting schemas</span></a>:</p>
<ul class="simple">
<li>You can specify which <a class="reference internal" href="nesting.html#specifying-nested-fields"><span>subset of fields</span></a> to include on nested schemas.</li>
<li><a class="reference internal" href="nesting.html#two-way-nesting"><span>Two-way nesting</span></a>. Two different schemas can nest each other.</li>
<li><a class="reference internal" href="nesting.html#self-nesting"><span>Self-nesting</span></a>. A schema can be nested within itself.</li>
</ul>
</div>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<p class="logo">
  <a href="index.html">
    <img class="logo" src="_static/marshmallow-logo.png" alt="Logo"/>
    
  </a>
</p>



<p class="blurb">Object serialization and deserialization, lightweight and fluffy.</p>



<p>
<iframe src="https://ghbtns.com/github-btn.html?user=marshmallow-code&repo=marshmallow&type=watch&count=true&size=large"
  allowtransparency="true" frameborder="0" scrolling="0" width="200px" height="35px"></iframe>
</p>



<h3>Useful Links</h3>
<ul>
  
    <li><a href="http://pypi.python.org/pypi/marshmallow">marshmallow @ PyPI</a></li>
  
    <li><a href="http://github.com/marshmallow-code/marshmallow">marshmallow @ GitHub</a></li>
  
    <li><a href="http://github.com/marshmallow-code/marshmallow/issues">Issue Tracker</a></li>
  
</ul>
  <h3><a href="index.html">Table Of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">Why marshmallow?</a><ul>
<li><a class="reference internal" href="#agnostic">Agnostic.</a></li>
<li><a class="reference internal" href="#concise-familiar-syntax">Concise, familiar syntax.</a></li>
<li><a class="reference internal" href="#class-based-schemas-allow-for-code-reuse-and-configuration">Class-based schemas allow for code reuse and configuration.</a></li>
<li><a class="reference internal" href="#consistency-meets-flexibility">Consistency meets flexibility.</a></li>
<li><a class="reference internal" href="#context-aware-serialization">Context-aware serialization.</a></li>
<li><a class="reference internal" href="#advanced-schema-nesting">Advanced schema nesting.</a></li>
</ul>
</li>
</ul>
<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="api_reference.html" title="previous chapter">API Reference</a></li>
      <li>Next: <a href="changelog.html" title="next chapter">Changelog</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3>Quick search</h3>
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" />
      <input type="submit" value="Go" />
      <input type="hidden" name="check_keywords" value="yes" />
      <input type="hidden" name="area" value="default" />
    </form>
    <p class="searchtip" style="font-size: 90%">
    Enter search terms or a module, class or function name.
    </p>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>
<h3>Donate</h3>
<p>
Consider supporting the authors on <a href="https://www.gratipay.com/">Gratipay</a>:
<script data-gratipay-username="sloria"
        data-gratipay-widget="button"
        src="//gttp.co/v1.js"></script>
</p>

        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy; 2015 <a href="http://stevenloria.com">Steven Loria</a>.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 1.3.1</a>.
      Theme based on <a href="https://github.com/sloria/alabaster">Alabaster 0.7.6</a>.
      
    </div>

    
    <a href="https://github.com/marshmallow-code/marshmallow" class="github">
        <img style="position: absolute; top: 0; right: 0; border: 0;" src="https://s3.amazonaws.com/github/ribbons/forkme_right_darkblue_121621.png" alt="Fork me on GitHub"  class="github"/>
    </a>
    

    
  </body>
</html>